package ru.tsc.mordovina.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "about";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}
