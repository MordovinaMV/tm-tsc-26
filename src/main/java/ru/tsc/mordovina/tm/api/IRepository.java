package ru.tsc.mordovina.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    void remove(E entity);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    boolean existsById(@NotNull String id);

    boolean existsByIndex(@NotNull Integer index);

    void clear();

    @NotNull
    E findById(@NotNull String id);

    @Nullable
    E findByIndex(@NotNull Integer index);

    @Nullable
    E removeById(@NotNull String id);

    @Nullable
    E removeByIndex(@NotNull Integer index);

    @NotNull
    Integer getSize();

}
